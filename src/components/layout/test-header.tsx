'use client'

import { useAuth } from '@/hooks/useAuth'
import {
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarItem,
  Skeleton
} from '@nextui-org/react'
import dynamic from 'next/dynamic'
import Link from 'next/link'
import { usePathname } from 'next/navigation'
import { useState } from 'react'

import { useGetUserInfoQuery } from '@/store/services/auth.api.service'

import { HeaderLogoIcon } from '../../../public/icons/HeaderLogo'
import MobileMenu from './mobile-menu'

const ProfileIndicator = dynamic(() => import('./profile-indicator'), {
  ssr: false,
  loading() {
    return <Skeleton className='h-8 w-[200px] rounded-lg' />
  }
})

const TestHeader = () => {
  const { isAuth } = useAuth()
  const { data } = useGetUserInfoQuery(null, { skip: !isAuth })
  const pathname = usePathname()

  const isMain = pathname === '/'

  const [isMenuOpen, setIsMenuOpen] = useState(false)

  return (
    <Navbar
      isBordered
      isBlurred
      onMenuOpenChange={setIsMenuOpen}
      classNames={{ wrapper: 'max-w-[1280px] px-4' }}
    >
      <NavbarBrand>
        <div className='flex items-center gap-4'>
          <MobileMenu isAuth={isAuth} isMenuOpen={isMenuOpen} />
          <HeaderLogoIcon />
        </div>
      </NavbarBrand>
      {isMain && (
        <NavbarContent className='sm:hidden flex gap-4' justify='center'>
          <NavbarItem>
            <Link color='foreground' href='#'>
              О нас
            </Link>
          </NavbarItem>
          <NavbarItem isActive>
            <Link href='#' aria-current='page'>
              Преимущества
            </Link>
          </NavbarItem>
          <NavbarItem>
            <Link color='foreground' href='#'>
              Отзывы
            </Link>
          </NavbarItem>
          <NavbarItem>
            <Link color='foreground' href='#'>
              Тарифы
            </Link>
          </NavbarItem>
        </NavbarContent>
      )}
      <NavbarContent justify='end'>
        <ProfileIndicator isAuth={isAuth} fullName={data?.fullName as string} />
      </NavbarContent>
    </Navbar>
  )
}

export default TestHeader
