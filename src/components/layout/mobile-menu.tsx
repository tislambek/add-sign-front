import useSignOut from '@/hooks/useSignOut'
import {
  Button,
  NavbarItem,
  NavbarMenu,
  NavbarMenuItem,
  NavbarMenuToggle
} from '@nextui-org/react'
import Link from 'next/link'
import { FC } from 'react'
import { GoSignOut } from 'react-icons/go'

interface IProps {
  isMenuOpen: boolean
  isAuth: boolean
}

const menuItems = ['О нас', 'Преимущества', 'Отзывы', 'Тарифы']

const MobileMenu: FC<IProps> = ({ isMenuOpen, isAuth }) => {
  const { handleSignOut, isLoading } = useSignOut()
  return (
    <>
      <NavbarMenuToggle
        aria-label={isMenuOpen ? 'Close menu' : 'Open menu'}
        className='sm:!block hidden'
      />
      <NavbarMenu className='flex h-full flex-col justify-between py-5'>
        <div className='flex flex-col gap-2'>
          {menuItems.map((item, index) => (
            <NavbarMenuItem key={`${item}-${index}`}>
              <Link className='w-full hover:text-primary' href='#'>
                {item}
              </Link>
            </NavbarMenuItem>
          ))}
        </div>
        <NavbarItem>
          {(!isAuth && (
            <div className='grid grid-cols-2 gap-2'>
              <Button variant='shadow' as={Link} color='primary' href='/auth'>
                Регистрация
              </Button>
              <Button variant='shadow' as={Link} href='/auth?isLogin=true'>
                Войти
              </Button>
            </div>
          )) || (
            <Button
              isLoading={isLoading}
              onPress={handleSignOut}
              fullWidth
              color='primary'
              variant='shadow'
              endContent={<GoSignOut size={18} />}
            >
              Выйти
            </Button>
          )}
        </NavbarItem>
      </NavbarMenu>
    </>
  )
}

export default MobileMenu
