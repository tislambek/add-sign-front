import useSignOut from '@/hooks/useSignOut'
import { Avatar, Button, NavbarItem } from '@nextui-org/react'
import Link from 'next/link'
import { FC } from 'react'
import { GoSignOut } from 'react-icons/go'

interface IProps {
  isAuth: boolean
  fullName: string
}

const ProfileIndicator: FC<IProps> = ({ isAuth, fullName }) => {
  const { handleSignOut, isLoading } = useSignOut()
  return (
    <NavbarItem>
      {(!isAuth && (
        <div className='flex items-center gap-2 sm:hidden'>
          <Button as={Link} color='primary' href='/auth'>
            Регистрация
          </Button>
          <Button as={Link} href='/auth?isLogin=true'>
            Войти
          </Button>
        </div>
      )) || (
        <div className='flex items-center gap-2'>
          <div className='p-1 rounded-full border border-primary min-sm:pr-2 flex items-center gap-2'>
            <Avatar name={fullName?.[0]} size='sm' />
            <p className='text-sm sm:hidden'>{fullName}</p>
          </div>
          <Button
            isLoading={isLoading}
            onPress={handleSignOut}
            className='sm:hidden'
            endContent={<GoSignOut size={18} />}
          >
            Выйти
          </Button>
        </div>
      )}
    </NavbarItem>
  )
}

export default ProfileIndicator
