import { FC, PropsWithChildren } from 'react'

import TestHeader from './test-header'

const Layout: FC<PropsWithChildren> = ({ children }) => {
  return (
    <>
      <TestHeader />
      <main className='w-full min-h-[calc(100vh_-_64px)] h-full bg-[#f8f8f8]'>
        {children}
      </main>
    </>
  )
}

export default Layout
