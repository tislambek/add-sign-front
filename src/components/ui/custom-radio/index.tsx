import { Radio, RadioProps, cn } from '@nextui-org/react'
import { FC } from 'react'

interface IProps extends RadioProps {}

const CustomRadio: FC<IProps> = props => {
  const { children, ...otherProps } = props

  return (
    <Radio
      {...otherProps}
      classNames={{
        base: cn(
          'inline-flex m-0 bg-content1 hover:bg-content2 items-center justify-between',
          'flex-row-reverse max-w-[300px] cursor-pointer rounded-lg gap-4 p-2 border-1 border-default-200',
          'data-[selected=true]:border-primary'
        )
      }}
    >
      <span className='text-sm'>{children}</span>
    </Radio>
  )
}

export default CustomRadio
