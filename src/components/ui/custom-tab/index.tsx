const CustomTab = ({
  activeTab,
  handleTabClick,
  tabContent
}: {
  activeTab: number
  handleTabClick: (index: number) => void
  tabContent: Array<string>
}) => {
  return (
    <div className='flex h-[40px] relative bg-[#f4f4f5] shadow-small rounded-medium w-fit'>
      {tabContent.map((tab, index) => (
        <div
          className='w-full h-full flex items-center justify-center'
          onClick={() => handleTabClick(index)}
          key={tab}
        >
          <label
            htmlFor={`radio-${index + 1}`}
            className={`flex text-sm items-center justify-center w-[120px] font-medium rounded-medium cursor-pointer transition-colors duration-150 text-[#3D3F44]`}
          >
            {tab}
          </label>
        </div>
      ))}
      <span
        className='absolute text-sm flex items-center justify-center top-0 left-0 h-[40px] border-2 font-medium border-[#f4f4f5] w-[120px] bg-[#fefefe] rounded-medium transition-transform duration-250'
        style={{
          transform: `translateX(${activeTab * 100}%)`
        }}
      >
        {tabContent[activeTab]}
      </span>
    </div>
  )
}

export default CustomTab
