import {
  Button,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  useDisclosure
} from '@nextui-org/react'
import { FC } from 'react'

interface IProps {
  cms: string
}

const ModalViewCms: FC<IProps> = ({ cms }) => {
  const { isOpen, onClose, onOpen } = useDisclosure()
  return (
    <>
      <div className='flex items-center gap-1 text-sm'>
        {cms.substring(0, 12)}{' '}
        <p
          onClick={onOpen}
          className='underline hover:text-primary cursor-pointer'
        >
          ..больше
        </p>
      </div>
      <Modal
        scrollBehavior='inside'
        isOpen={isOpen}
        onClose={onClose}
        backdrop='blur'
        size='lg'
        placement='center'
      >
        <ModalContent>
          <ModalHeader>Подпись</ModalHeader>
          <ModalBody>
            <div className='break-all pb-7'>{cms}</div>
          </ModalBody>
          <ModalFooter>
            <Button variant='light' onPress={onClose}>
              Закрыть
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}

export default ModalViewCms
