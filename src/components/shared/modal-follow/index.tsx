import useInitializeRutoken from '@/hooks/useInitializeRutoken'
import {
  Button,
  Input,
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  Select,
  SelectItem,
  Spinner
} from '@nextui-org/react'
import { useRouter } from 'next/navigation'
import { FC, useEffect, useState } from 'react'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import toast from 'react-hot-toast'
import { CiWarning } from 'react-icons/ci'

import { getError } from '@/lib/helper'

import {
  useDocumentSignMutation,
  usePostDocumentMutation,
  useUploadAttachmentMutation
} from '@/store/services/documents.service'

import { IFormValuesCreateDocument } from '../../../screens/create-document'

interface IFormValues {
  pin: string
  device: number
  certificate: string
}

interface IProps {
  withoutCreateDocument?: boolean
  isOpen: boolean
  onClose: () => void
  documentData?: IFormValuesCreateDocument
  documentIds?: { id: number; key: string }[]
  isShared?: boolean
  actionAfterSign?: Function
}

const ModalFollow: FC<IProps> = ({
  isOpen,
  onClose,
  documentData,
  withoutCreateDocument = false,
  documentIds = [],
  isShared = false,
  actionAfterSign
}) => {
  const router = useRouter()
  const {
    register,
    watch,
    control,
    handleSubmit,
    formState: { errors }
  } = useForm<IFormValues>()

  const [uploadAttachment] = useUploadAttachmentMutation()
  const [createDocument] = usePostDocumentMutation()
  const [documentSign] = useDocumentSignMutation()

  const [isLoadingFollow, setIsLoadingFollow] = useState(false)
  const [isExpiration, setIsExpiration] = useState(false)

  const {
    isExtensionInstalled,
    isLoadingPlugin,
    isPluginInstalled,
    plugin,
    deviceLabels,
    certificates,
    isLoadingGetDropdowns
  } = useInitializeRutoken()

  const onSubmit: SubmitHandler<IFormValues> = async ({
    device,
    pin,
    certificate
  }) => {
    try {
      let incomingDocumentIds = documentIds

      setIsLoadingFollow(true)
      await plugin?.login(device, pin)

      if (!withoutCreateDocument) {
        const resultUpload = await uploadAttachment(
          documentData?.attachment as File
        ).unwrap()
        const responseDocument = await createDocument({
          attachmentId: resultUpload.id,
          comment: documentData?.comment as string,
          name: documentData?.name as string
        }).unwrap()

        incomingDocumentIds = [
          { id: responseDocument.id, key: resultUpload.data }
        ]
      }

      const requestedData = await Promise.all(
        incomingDocumentIds.map(async item => ({
          cms: await plugin?.sign(
            device,
            certificate,
            item.key,
            plugin.DATA_FORMAT_PLAIN,
            {}
          ),
          documentId: item.id
        }))
      )
      await documentSign({ body: requestedData, isShared }).unwrap()
      toast.success('успешно')
      actionAfterSign?.()
      if (!withoutCreateDocument) {
        router.push('/personal-area/list-of-documents')
      }
      setIsLoadingFollow(false)
      onClose()
    } catch (error) {
      setIsLoadingFollow(false)
      toast.error(getError(error))
    }
  }

  const certificate = watch('certificate')
  const device = watch('device')

  useEffect(() => {
    if (certificate !== undefined && device !== undefined) {
      ;(async () => {
        const parsedCertificate = await plugin?.parseCertificate(
          device,
          certificate
        )
        setIsExpiration(
          new Date(parsedCertificate?.validNotAfter?.split('T')?.[0]) <
            new Date()
        )
      })()
    }
  }, [certificate, device])

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      backdrop='blur'
      size='lg'
      placement='center'
    >
      <ModalContent>
        {onClose => (
          <>
            <ModalHeader className='flex flex-col gap-1'></ModalHeader>
            <ModalBody className='pb-7'>
              <form
                onSubmit={handleSubmit(onSubmit)}
                className='w-full flex flex-col gap-5 relative'
              >
                {(isLoadingPlugin ||
                  !isExtensionInstalled ||
                  !isPluginInstalled) && (
                  <div className='w-full backdrop-blur-sm p-4 absolute top-0 left-0 h-full flex items-center justify-center bg-black/10 z-[1000] rounded-medium'>
                    {isLoadingPlugin && <Spinner />}
                    {!isLoadingPlugin && (
                      <div className='w-full text-center text-danger flex flex-col gap-3 p-2 rounded-medium z-10'>
                        {!isExtensionInstalled && (
                          <p>
                            Не удаётся най ти расширение 'Адаптер Рутокен
                            Плагина'
                          </p>
                        )}
                        {!isPluginInstalled && <p>Не удаётся найти Плагин</p>}
                      </div>
                    )}
                  </div>
                )}
                {isExpiration && (
                  <div className='w-full flex items-center gap-2 rounded-small px-4 py-2 text-sm border text-warning border-warning bg-warning-50'>
                    <CiWarning className='text-danger' size={20} /> Срок этого
                    сертификата истек
                  </div>
                )}
                <Controller
                  control={control}
                  name='device'
                  rules={{ required: 'Пожалуйста, введите это поле' }}
                  render={({ field: { value, onChange, ref } }) => {
                    return (
                      <Select
                        ref={ref}
                        isDisabled={isLoadingGetDropdowns}
                        isLoading={isLoadingGetDropdowns}
                        classNames={{
                          value:
                            'text-gray-400 group-data-[has-value=true]:text-default-foreground'
                        }}
                        variant='bordered'
                        labelPlacement='outside'
                        label='Устройство'
                        placeholder='Выберите устройство'
                        value={value}
                        onChange={e => onChange(Number(e.target.value))}
                        errorMessage={errors?.device?.message}
                        isInvalid={!!errors?.device?.message}
                      >
                        {deviceLabels.map(item => (
                          <SelectItem key={item.value} value={item.value}>
                            {item.label}
                          </SelectItem>
                        ))}
                      </Select>
                    )
                  }}
                />

                <Controller
                  control={control}
                  name='certificate'
                  rules={{ required: 'Пожалуйста, введите это поле' }}
                  render={({ field: { value, onChange, ref } }) => {
                    return (
                      <Select
                        ref={ref}
                        isDisabled={isLoadingGetDropdowns}
                        isLoading={isLoadingGetDropdowns}
                        classNames={{
                          value:
                            'text-gray-400 group-data-[has-value=true]:text-default-foreground'
                        }}
                        variant='bordered'
                        labelPlacement='outside'
                        label='Сертификат'
                        placeholder='Выберите сертификат'
                        value={value}
                        onChange={e => onChange(e.target.value)}
                        errorMessage={errors?.certificate?.message}
                        isInvalid={!!errors?.certificate?.message}
                      >
                        {certificates.map(item => (
                          <SelectItem key={item.value} value={item.value}>
                            {item.label}
                          </SelectItem>
                        ))}
                      </Select>
                    )
                  }}
                />

                <Input
                  classNames={{ input: 'placeholder:text-gray-400' }}
                  labelPlacement='outside'
                  label='PIN-код'
                  placeholder='Введите PIN-код'
                  variant='bordered'
                  {...register('pin', {
                    required: 'Пожалуйста, введите это поле'
                  })}
                  errorMessage={errors?.pin?.message}
                  isInvalid={!!errors?.pin?.message}
                />
                <Button
                  isLoading={isLoadingFollow}
                  type='submit'
                  color='primary'
                  variant='shadow'
                >
                  Подписать
                </Button>
              </form>
            </ModalBody>
          </>
        )}
      </ModalContent>
    </Modal>
  )
}

export default ModalFollow
