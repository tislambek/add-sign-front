'use client'

import { Viewer, Worker } from '@react-pdf-viewer/core'
import '@react-pdf-viewer/core/lib/styles/index.css'
import '@react-pdf-viewer/default-layout/lib/styles/index.css'
import { FC } from 'react'

export interface IViewerWrapperProps {
  fileUrl: string
}

const ViewerPdf: FC<IViewerWrapperProps> = ({ fileUrl }) => {
  return (
    <Worker workerUrl='https://unpkg.com/pdfjs-dist@3.11.174/build/pdf.worker.min.js'>
      <Viewer fileUrl={fileUrl} />
    </Worker>
  )
}

export default ViewerPdf
