'use client'

import rutoken from '@/plugins/rutoken'
import { useEffect, useState } from 'react'

import { IPlugin } from '@/store/models/interfaces/general.interfaces'

interface ILabel {
  label: string
  value: number | string
}

const useInitializeRutoken = () => {
  const [plugin, setPlugin] = useState<IPlugin | null>(null)
  const [isLoadingPlugin, setIsLoadingPlugin] = useState(false)
  const [isLoadingGetDropdowns, setIsLoadingDevices] = useState(false)
  const [isExtensionInstalled, setExtensionInstalled] = useState(false)
  const [isPluginInstalled, setPluginInstalled] = useState(false)
  const [inValidRutoken, setInValidRutoken] = useState(false)

  const [deviceLabels, setDeviceLabels] = useState<ILabel[]>([])
  const [certificates, setCertificates] = useState<ILabel[]>([])

  const getLabelDevices = async (deviceId: number) => {
    const SERIAL = await plugin?.getDeviceInfo(
      deviceId,
      plugin.TOKEN_INFO_SERIAL
    )
    const LABEL = await plugin?.getDeviceInfo(deviceId, plugin.TOKEN_INFO_LABEL)
    return LABEL.replace('<no label>', `<${SERIAL}>`)
  }

  const getAllRutokenDropdowns = async () => {
    try {
      setIsLoadingDevices(true)
      let devices: number[] = await plugin?.enumerateDevices()
      if (devices.length > 0) {
        setInValidRutoken(false)
        let deviceLabels = await Promise.all(
          devices.map(async item => ({
            label: await getLabelDevices(item),
            value: item
          }))
        )
        let certificates: string[] = await plugin?.enumerateCertificates(
          devices[0],
          plugin.CERT_CATEGORY_UNSPEC
        )
        setCertificates(
          certificates.map(item => ({ label: item, value: item }))
        )
        setDeviceLabels(deviceLabels)
      } else {
        setInValidRutoken(true)
        throw new Error('Рутокен не найден')
      }
      setIsLoadingDevices(false)
    } catch (error) {
      setIsLoadingDevices(false)
    }
  }

  const initializeRutoken = async () => {
    setIsLoadingPlugin(true)
    try {
      await rutoken.ready
      if (typeof window !== 'undefined' && (window as any).chrome) {
        let extensionInstalled = await rutoken.isExtensionInstalled()
        setExtensionInstalled(extensionInstalled)
      } else {
        setExtensionInstalled(true)
      }

      let pluginInstalled = await rutoken.isPluginInstalled()
      setPluginInstalled(pluginInstalled)

      let pluginObject = await rutoken.loadPlugin()
      setPlugin(pluginObject)
      setIsLoadingPlugin(false)
    } catch (error) {
      setIsLoadingPlugin(false)
    }
  }

  useEffect(() => {
    initializeRutoken()
  }, [])

  useEffect(() => {
    if (plugin) getAllRutokenDropdowns()
  }, [plugin])

  return {
    isLoadingPlugin,
    isExtensionInstalled,
    isPluginInstalled,
    plugin,
    isLoadingGetDropdowns,
    deviceLabels,
    certificates,
    inValidRutoken
  }
}

export default useInitializeRutoken
