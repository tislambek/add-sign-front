'use client'

import { useRouter } from 'next/navigation'
import { useCallback } from 'react'
import toast from 'react-hot-toast'
import { useDispatch } from 'react-redux'

import { getError } from '@/lib/helper'

import { useSignOutMutation } from '@/store/services/auth.api.service'
import { logOut } from '@/store/slices/auth-slice'

const useSignOut = () => {
  const router = useRouter()
  const dispatch = useDispatch()
  const [signOut, { isLoading }] = useSignOutMutation()

  const handleSignOut = useCallback(async () => {
    try {
      // await signOut().unwrap()
      dispatch(logOut())
      router.push('/auth?isLogin=true')
    } catch (error) {
      toast.error(getError(error))
    }
  }, [])

  return { handleSignOut, isLoading }
}

export default useSignOut
