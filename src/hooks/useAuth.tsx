import { RootState } from '@/store'
import { useSelector } from 'react-redux'

export const useAuth = (): { isAuth: boolean } => {
  const { accessToken } = useSelector((state: RootState) => state.auth)
  const isAuth = Boolean(accessToken)

  return { isAuth }
}
