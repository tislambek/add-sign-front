'use client'

import { Button } from '@nextui-org/react'
import Link from 'next/link'
import { useSearchParams } from 'next/navigation'

export const animals = [
  {
    label: 'Cat',
    value: 'cat',
    description: 'The second most popular pet in the world'
  },
  {
    label: 'Dog',
    value: 'dog',
    description: 'The most popular pet in the world'
  },
  {
    label: 'Elephant',
    value: 'elephant',
    description: 'The largest land animal'
  },
  { label: 'Lion', value: 'lion', description: 'The king of the jungle' },
  { label: 'Tiger', value: 'tiger', description: 'The largest cat species' },
  {
    label: 'Giraffe',
    value: 'giraffe',
    description: 'The tallest land animal'
  }
]

const MethodAuth = () => {
  const params = useSearchParams()

  const isLogin = params.get('isLogin')

  return (
    <div className='bg-white min-h-[calc(100vh_-_104px)] w-full flex justify-center items-center'>
      <div className='w-full rounded-medium max-w-[500px] shadow-md py-[50px] px-[25px]'>
        <h1 className='text-[24px] text-center font-medium'>
          {isLogin ? 'Войдите в систему через' : 'Регистрация в систему через'}
        </h1>
        <div className='w-full flex flex-col gap-5 mt-10'>
          <Button
            as={Link}
            size='lg'
            variant='bordered'
            color='primary'
            className='text-black'
            href={
              isLogin
                ? '/auth?isLoginByRutoken=true'
                : '/auth?isSignUpByRutoken=true'
            }
          >
            {isLogin ? 'Войти' : 'Регистрация'} через RUTOKEN
          </Button>
          <Button
            size='lg'
            variant='bordered'
            color='primary'
            className='text-black'
          >
            {isLogin ? 'Войти' : 'Регистрация'} через ЕСИА
          </Button>
          <Link
            className='text-center text-primary hover:underline'
            href={isLogin ? '/auth' : '/auth?isLogin=true'}
          >
            {isLogin
              ? 'У вас нету аккаунта? Пройдите регистрацию.'
              : 'У вас есть аккаунт? Вход'}
          </Link>
        </div>
      </div>
    </div>
  )
}

export default MethodAuth
