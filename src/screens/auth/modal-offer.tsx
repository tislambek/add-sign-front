'use client'

import {
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  useDisclosure
} from '@nextui-org/react'
import dynamic from 'next/dynamic'
import React from 'react'

const ViewerPdf = dynamic(() => import('@/components/shared/viewer-pdf'), {
  ssr: false
})

const ModalOffer = () => {
  const { isOpen, onClose, onOpen } = useDisclosure()
  return (
    <>
      <p
        onClick={onOpen}
        className='text-primary hover:underline cursor-pointer'
      >
        ссылка
      </p>
      <Modal
        scrollBehavior='inside'
        isOpen={isOpen}
        onClose={onClose}
        backdrop='blur'
        size='3xl'
        placement='center'
      >
        <ModalContent>
          <ModalHeader className='flex flex-col gap-1'></ModalHeader>
          <ModalBody className='pb-7'>
            <ViewerPdf fileUrl='/pdf/public_offer_v2.pdf' />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  )
}

export default ModalOffer
