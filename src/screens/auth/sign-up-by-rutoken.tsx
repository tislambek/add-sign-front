'use client'

import useInitializeRutoken from '@/hooks/useInitializeRutoken'
import {
  Button,
  Checkbox,
  Input,
  Select,
  SelectItem,
  Spinner
} from '@nextui-org/react'
import { useRouter } from 'next/navigation'
import { useEffect, useState } from 'react'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import toast from 'react-hot-toast'
import { CiWarning } from 'react-icons/ci'

import { getError, makeObject } from '@/lib/helper'

import {
  useAuthStartMutation,
  useSignUpMutation
} from '@/store/services/auth.api.service'

import ModalOffer from './modal-offer'

interface IFormValues {
  pin: string
  device: number
  certificate: string
  readTheOffer: boolean
}

const SignUpByRutoken = () => {
  const router = useRouter()
  const {
    register,
    control,
    handleSubmit,
    watch,
    formState: { errors }
  } = useForm<IFormValues>()

  const {
    isExtensionInstalled,
    isLoadingPlugin,
    isPluginInstalled,
    plugin,
    deviceLabels,
    certificates,
    isLoadingGetDropdowns,
    inValidRutoken
  } = useInitializeRutoken()

  const [isLoadingSignUp, setIsLoadingSignUp] = useState(false)
  const [user, setUser] = useState<{ [key: string]: string } | null>(null)
  const [isExpiration, setIsExpiration] = useState(false)

  const [authStart, { data }] = useAuthStartMutation()
  const [signUp] = useSignUpMutation()

  const onSubmit: SubmitHandler<IFormValues> = async ({
    device,
    pin,
    certificate,
    readTheOffer
  }) => {
    try {
      setIsLoadingSignUp(true)
      const isDeviceLoggedIn = await plugin?.getDeviceInfo(
        device,
        plugin.TOKEN_INFO_IS_LOGGED_IN
      )
      if (!isDeviceLoggedIn) {
        await plugin?.login(device, pin)
      }
      const signedData = await plugin?.sign(
        device,
        certificate,
        data?.randomToken,
        plugin.DATA_FORMAT_PLAIN,
        {}
      )
      await signUp({
        registrationKey: data?.key as string,
        sign: signedData,
        agreeWithOffer: readTheOffer
      }).unwrap()
      router.push('/auth?isLoginByRutoken=true')
      toast.success('Успешно')
      setIsLoadingSignUp(false)
    } catch (error) {
      console.log(error)

      setIsLoadingSignUp(false)
      toast.error(getError(error))
    }
  }

  useEffect(() => {
    authStart().unwrap()
  }, [])

  const certificate = watch('certificate')
  const device = watch('device')

  useEffect(() => {
    if (certificate !== undefined && device !== undefined) {
      ;(async () => {
        const parsedCertificate = await plugin?.parseCertificate(
          device,
          certificate
        )

        setIsExpiration(
          new Date(parsedCertificate?.validNotAfter?.split('T')?.[0]) <
            new Date()
        )
        setUser(makeObject(parsedCertificate.subject))
      })()
    }
  }, [certificate, device])

  return (
    <div className='bg-white min-h-[calc(100vh_-_104px)] w-full flex justify-center items-center'>
      <div className='w-full rounded-medium max-w-[500px] shadow-md py-[50px] px-[25px]'>
        <h1 className='text-[24px] text-center font-medium'>
          Регистрация через RUTOKEN
        </h1>

        <form
          onSubmit={handleSubmit(onSubmit)}
          className='w-full flex flex-col gap-5 mt-10 relative'
        >
          {(isLoadingPlugin ||
            !isExtensionInstalled ||
            !isPluginInstalled ||
            inValidRutoken) && (
            <div className='w-full backdrop-blur-sm p-4 absolute top-0 left-0 h-full flex items-center justify-center bg-black/10 z-[1000] rounded-medium'>
              {isLoadingPlugin && <Spinner />}
              {!isLoadingPlugin && (
                <div className='w-full text-center text-danger flex flex-col gap-3 p-2 rounded-medium z-10'>
                  {!isExtensionInstalled && (
                    <p>
                      Не удаётся най ти расширение 'Адаптер Рутокен Плагина'
                    </p>
                  )}
                  {!isPluginInstalled && <p>Не удаётся найти Плагин</p>}
                  {inValidRutoken && <p>Рутокен не найден</p>}
                </div>
              )}
            </div>
          )}

          {isExpiration && (
            <div className='w-full flex items-center gap-2 rounded-small px-4 py-2 text-sm border text-warning border-warning bg-warning-50'>
              <CiWarning className='text-danger' size={20} /> Срок этого
              сертификата истек
            </div>
          )}

          <Controller
            control={control}
            name='device'
            rules={{ required: 'Пожалуйста, введите это поле' }}
            render={({ field: { value, onChange, ref } }) => {
              return (
                <Select
                  ref={ref}
                  isDisabled={isLoadingGetDropdowns}
                  isLoading={isLoadingGetDropdowns}
                  classNames={{
                    value:
                      'text-gray-400 group-data-[has-value=true]:text-default-foreground'
                  }}
                  variant='bordered'
                  labelPlacement='outside'
                  label='Устройство'
                  placeholder='Выберите устройство'
                  value={value}
                  onChange={e => onChange(Number(e.target.value))}
                  errorMessage={errors?.device?.message}
                  isInvalid={!!errors?.device?.message}
                >
                  {deviceLabels.map(item => (
                    <SelectItem key={item.value} value={item.value}>
                      {item.label}
                    </SelectItem>
                  ))}
                </Select>
              )
            }}
          />

          <Controller
            control={control}
            name='certificate'
            rules={{ required: 'Пожалуйста, введите это поле' }}
            render={({ field: { value, onChange, ref } }) => {
              return (
                <Select
                  ref={ref}
                  isDisabled={isLoadingGetDropdowns}
                  isLoading={isLoadingGetDropdowns}
                  classNames={{
                    value:
                      'text-gray-400 group-data-[has-value=true]:text-default-foreground'
                  }}
                  variant='bordered'
                  labelPlacement='outside'
                  label='Сертификат'
                  placeholder='Выберите сертификат'
                  value={value}
                  onChange={e => onChange(e.target.value)}
                  errorMessage={errors?.certificate?.message}
                  isInvalid={!!errors?.certificate?.message}
                >
                  {certificates.map(item => (
                    <SelectItem key={item.value} value={item.value}>
                      {item.label}
                    </SelectItem>
                  ))}
                </Select>
              )
            }}
          />

          <Input
            classNames={{ input: 'placeholder:text-gray-400' }}
            labelPlacement='outside'
            label='ИНН'
            placeholder='Введите ИНН'
            variant='bordered'
            value={user?.serialNumber || ''}
            readOnly
          />

          <Input
            classNames={{ input: 'placeholder:text-gray-400' }}
            labelPlacement='outside'
            label='ФИО пользователя (ФИО Владельца ЭЦП)'
            placeholder='Введите ФИО'
            variant='bordered'
            value={user?.commonName || ''}
            readOnly
          />

          <Input
            classNames={{ input: 'placeholder:text-gray-400' }}
            labelPlacement='outside'
            label='ИНН организации'
            placeholder='ИНН организации'
            variant='bordered'
            value={user?.INN || ''}
            readOnly
          />

          <Input
            classNames={{ input: 'placeholder:text-gray-400' }}
            labelPlacement='outside'
            label='Наименование организации'
            placeholder='Наименование организации'
            variant='bordered'
            value={user?.organizationName || ''}
            readOnly
          />

          <Input
            classNames={{ input: 'placeholder:text-gray-400' }}
            labelPlacement='outside'
            label='PIN-код'
            placeholder='Введите PIN-код'
            variant='bordered'
            {...register('pin', { required: 'Пожалуйста, введите это поле' })}
            errorMessage={errors?.pin?.message}
            isInvalid={!!errors?.pin?.message}
          />

          <Controller
            control={control}
            rules={{ required: 'Ознакомтесь с офертой' }}
            name='readTheOffer'
            render={({ field: { onChange, value } }) => (
              <Checkbox
                onChange={e => onChange(e.target.checked)}
                checked={value}
                isInvalid={!!errors?.readTheOffer?.message}
              >
                <div className='text-sm flex items-center gap-2'>
                  {errors?.readTheOffer?.message || 'Ознакомлен с офертой'}{' '}
                  <ModalOffer />
                </div>
              </Checkbox>
            )}
          />

          <Button
            isLoading={isLoadingSignUp}
            type='submit'
            color='primary'
            variant='shadow'
          >
            Регистрироваться
          </Button>
        </form>
      </div>
    </div>
  )
}

export default SignUpByRutoken
