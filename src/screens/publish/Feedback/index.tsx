'use client'

import { Avatar } from '@nextui-org/react'
import Image from 'next/image'
import { useRef, useState } from 'react'
import { EffectCoverflow } from 'swiper/modules'
import { Swiper, SwiperSlide } from 'swiper/react'

import FeedbackLeft from '../../../../public/FeedbackSlug1.svg'
import FeedbackRight from '../../../../public/FeedbackSlug2.svg'
import { ReviewEllips } from '../../../../public/icons/ReviewEllips'

export const Reviews = () => {
  const [indexCurrent, setIndexCurrent] = useState(0)
  const swiperRef = useRef<any>(null)

  const handleSlideChange = (swiper: any) => {
    setIndexCurrent(swiper.activeIndex)
  }

  const REVIEW_DATA = [
    {
      name: 'Иван Иванович Петров',
      desc: 'Электронная подпись просто спасла мою жизнь! Теперь я могу подписывать документы и отправлять важные соглашения в считанные минуты, не выходя из дома. Это удивительно удобно и экономит мне уйму времени. Рекомендую всем, кто ценит свою эффективность!'
    },
    {
      name: 'Александр Владимирович Смирнов',
      desc: 'Электронная подпись - это просто находка для бизнеса! Мы используем её для подписания договоров с партнерами и клиентами. Это удобно, быстро и надёжно. Больше не нужно заботиться о безопасности документов и их хранении. Спасибо за такой замечательный инструмент!'
    },
    {
      name: 'Елена Сергеевна Козлова',
      desc: 'Использование электронной подписи сделало мою работу более организованной и безопасной. Теперь нет необходимости тратить время на печать, сканирование и отправку бумажных документов. Процесс подписи стал гораздо быстрее и прозрачнее. Очень доволен результатом!'
    }
  ]

  return (
    <div className='w-full h-[800px] rounded-[60px] bg-[#F8F8F8] mt-64 md:rounded-none md:h-[700px] md:mt-0'>
      <div className='container w-full h-full relative flex items-start justify-center m-auto p-20'>
        <Image
          src={FeedbackLeft}
          width={500}
          height={500}
          alt='FeedbackLeft'
          className='absolute w-[200px] left-0 md:hidden'
        />
        <Image
          src={FeedbackRight}
          width={500}
          height={500}
          alt='FeedbackRight'
          className='absolute w-[200px] right-0 md:hidden'
        />
        <div className='w-[60%] absolute'>
          <ReviewEllips />
        </div>
        <h3 className='text-black text-center text-5xl z-10 md:text-3xl'>
          Отзывы наших{' '}
          <span className='italic font-serif text-[#194BFA]'>клиентов</span>
        </h3>
        <div className='absolute top-[150px] w-full h-[650px] p-5'>
          <Swiper
            ref={swiperRef}
            slidesPerView={1}
            breakpoints={{
              1200: {
                slidesPerView: 3,
                spaceBetween: 100
              },
              768: {
                slidesPerView: 1,
                spaceBetween: 30
              }
            }}
            spaceBetween={100}
            initialSlide={1}
            modules={[EffectCoverflow]}
            onSlideChange={handleSlideChange}
            centeredSlides={true}
            effect={'coverflow'}
            coverflowEffect={{
              rotate: 0,
              stretch: 0,
              depth: 100,
              modifier: 3.5,
              slideShadows: false
            }}
          >
            {REVIEW_DATA.map((item: any, index: number) => (
              <SwiperSlide key={index}>
                <div className='w-[400px] h-[550px] relative my-2 md:w-[300px] md:h-[450px] m-auto'>
                  <div className='absolute top-0 w-[400px] h-[500px] bg-[#194BFA] rotate-[-3deg] rounded-[60px] md:w-[300px] md:h-[400px]'></div>
                  <div className='absolute top-5 left-3 h-[500px] w-[400px] rounded-[61px] bg-gradient-to-b from-[#2F80ED] via-[#E580F9] to-[#FF8226] p-[3px] md:w-[300px] md:h-[400px]'>
                    <div className='w-full h-full rounded-[60px] bg-white p-5 flex flex-col items-start justify-start gap-5'>
                      <div className='w-full flex items-center justify-start gap-5 mt-10 md:justify-center'>
                        <Avatar src='https://i.pravatar.cc/150?u=a042581f4e29026024d' />
                        <h1 className='md:text-[12px]'>{item.name}</h1>
                      </div>
                      <svg
                        width='23'
                        height='17'
                        viewBox='0 0 23 17'
                        fill='none'
                        xmlns='http://www.w3.org/2000/svg'
                      >
                        <path
                          d='M5.50027 6.77956C5.2557 6.77956 5.02099 6.81817 4.78739 6.85337C4.86306 6.58991 4.94093 6.32191 5.06596 6.08116C5.19099 5.7314 5.38621 5.42819 5.58033 5.12271C5.74265 4.79225 6.02889 4.56854 6.23947 4.28577C6.45991 4.01095 6.76042 3.82812 6.99841 3.59987C7.23201 3.36139 7.538 3.24215 7.78148 3.07408C8.03592 2.92305 8.25746 2.75611 8.49435 2.67662L9.0855 2.42452L9.60535 2.2008L9.07343 0L8.41868 0.163527C8.2092 0.218036 7.95366 0.281629 7.66303 0.357715C7.36581 0.414495 7.04886 0.570073 6.69571 0.712024C6.34695 0.87328 5.94335 0.982298 5.56826 1.24122C5.19099 1.48878 4.75558 1.69546 4.37173 2.02705C3.99993 2.36887 3.55137 2.66526 3.22015 3.1002C2.85823 3.50675 2.5007 3.93373 2.22322 4.41977C1.90188 4.8831 1.68363 5.39185 1.45331 5.89492C1.24493 6.398 1.07713 6.91242 0.94004 7.41209C0.680114 8.41369 0.56386 9.36533 0.518894 10.1796C0.481605 10.9949 0.503539 11.6729 0.549602 12.1635C0.566053 12.3951 0.596762 12.62 0.618697 12.7756L0.646115 12.9663L0.67463 12.9595C0.869697 13.903 1.31875 14.77 1.96983 15.4603C2.62092 16.1506 3.44744 16.6359 4.35378 16.8601C5.26012 17.0843 6.20925 17.0383 7.09136 16.7273C7.97347 16.4163 8.75253 15.853 9.33841 15.1027C9.92429 14.3524 10.2931 13.4457 10.402 12.4874C10.511 11.5292 10.3558 10.5586 9.95426 9.68787C9.55274 8.81716 8.92136 8.08194 8.13315 7.56727C7.34494 7.0526 6.43211 6.77949 5.50027 6.77956ZM17.5644 6.77956C17.3198 6.77956 17.0851 6.81817 16.8515 6.85337C16.9272 6.58991 17.005 6.32191 17.1301 6.08116C17.2551 5.7314 17.4503 5.42819 17.6444 5.12271C17.8067 4.79225 18.093 4.56854 18.3036 4.28577C18.524 4.01095 18.8245 3.82812 19.0625 3.59987C19.2961 3.36139 19.6021 3.24215 19.8456 3.07408C20.1 2.92305 20.3216 2.75611 20.5584 2.67662L21.1496 2.42452L21.6694 2.2008L21.1375 0L20.4828 0.163527C20.2733 0.218036 20.0178 0.281629 19.7271 0.357715C19.4299 0.414495 19.1129 0.570073 18.7598 0.712024C18.4121 0.874415 18.0074 0.982298 17.6324 1.24235C17.2551 1.48991 16.8197 1.69659 16.4358 2.02819C16.064 2.37001 15.6155 2.6664 15.2842 3.1002C14.9223 3.50675 14.5648 3.93373 14.2873 4.41977C13.966 4.8831 13.7477 5.39185 13.5174 5.89492C13.309 6.398 13.1412 6.91242 13.0041 7.41209C12.7442 8.41369 12.628 9.36533 12.583 10.1796C12.5457 10.9949 12.5676 11.6729 12.6137 12.1635C12.6301 12.3951 12.6609 12.62 12.6828 12.7756L12.7102 12.9663L12.7387 12.9595C12.9338 13.903 13.3828 14.77 14.0339 15.4603C14.685 16.1506 15.5115 16.6359 16.4179 16.8601C17.3242 17.0843 18.2733 17.0383 19.1555 16.7273C20.0376 16.4163 20.8166 15.853 21.4025 15.1027C21.9884 14.3524 22.3571 13.4457 22.4661 12.4874C22.5751 11.5292 22.4199 10.5586 22.0183 9.68787C21.6168 8.81716 20.9854 8.08194 20.1972 7.56727C19.409 7.0526 18.4962 6.77949 17.5644 6.77956Z'
                          fill='#194BFA'
                        />
                      </svg>
                      <div>
                        <p className='md:text-sm'>{item.desc}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </SwiperSlide>
            ))}
          </Swiper>
          <div className='w-full flex justify-center mt-[20px]'>
            <div className='w-fit flex justify-between items-center gap-3 cursor-pointer'>
              <div
                onClick={() => swiperRef.current.swiper.slidePrev()}
                className='active:translate-x-[-15px] transition-all duration-300'
              >
                <svg
                  width='24'
                  height='16'
                  viewBox='0 0 24 16'
                  fill='none'
                  xmlns='http://www.w3.org/2000/svg'
                >
                  <path
                    d='M0.792391 7.30121C0.402145 7.69201 0.402595 8.32517 0.793396 8.71542L7.16188 15.0749C7.55268 15.4651 8.18584 15.4647 8.57609 15.0739C8.96634 14.6831 8.96589 14.0499 8.57508 13.6596L2.91421 8.00681L8.56705 2.34594C8.9573 1.95514 8.95685 1.32197 8.56604 0.931725C8.17524 0.541478 7.54208 0.541928 7.15183 0.932729L0.792391 7.30121ZM23.4993 6.99219L1.49929 7.00781L1.50071 9.00781L23.5007 8.99219L23.4993 6.99219Z'
                    fill='#414141'
                  />
                </svg>
              </div>
              {Array.from({ length: REVIEW_DATA.length }).map(
                (item: any, index: number) => (
                  <div
                    key={index}
                    className={`bg-[#999999] w-2 h-2 rounded-full transition-all duration-300  ${
                      index === indexCurrent && ' scale-125 !bg-[#194BFA]'
                    }`}
                  />
                )
              )}
              <div
                onClick={() => swiperRef.current.swiper.slideNext()}
                className='active:translate-x-[15px] transition-all duration-300'
              >
                <svg
                  width='24'
                  height='16'
                  viewBox='0 0 24 16'
                  fill='none'
                  xmlns='http://www.w3.org/2000/svg'
                >
                  <path
                    d='M23.2071 8.70711C23.5976 8.31658 23.5976 7.68342 23.2071 7.29289L16.8431 0.928932C16.4526 0.538408 15.8195 0.538408 15.4289 0.928932C15.0384 1.31946 15.0384 1.95262 15.4289 2.34315L21.0858 8L15.4289 13.6569C15.0384 14.0474 15.0384 14.6805 15.4289 15.0711C15.8195 15.4616 16.4526 15.4616 16.8431 15.0711L23.2071 8.70711ZM0.5 9H22.5V7H0.5V9Z'
                    fill='#414141'
                  />
                </svg>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
