import { IntroCenter } from './IntroCenter'
import { IntroLeft } from './IntroLeft'
import { IntroRight } from './IntroRight'

export const Intro = () => {
	return (
		<div className='flex flex-col relative'>
			<div className='w-full h-[350px] absolute flex flex-col items-center justify-around md:h-[250px]'>
				<div className='flex flex-col items-center justify-center text-center'>
					<h1 className='text-[62px] md:text-[32px]'>Электронная подпись:</h1>
					<h2 className='text-[#194BFA] text-[58px] italic font-serif md:text-[32px]'>
						просто и безопасно
					</h2>
					<span className='w-[400px] mt-[20px] md:text-[12px] md:w-[250px]'>
						ADDSIGN - ваш надежный партнер в подписании документов онлайн в
						Кыргызстане
					</span>
				</div>
				<div className='w-[241px] h-[48px] px-3.5 py-2.5 border-2 border-[#2F80ED] text-[#2F80ED] font-medium rounded-2xl flex items-center justify-center cursor-pointer md:mt-[20px]'>
					Попробовать бесплатно
				</div>
			</div>
			<div className='flex items-end justify-between gap-10 mt-[250px] md:flex-col md:justify-center md:items-center md:mt-[300px]'>
				<IntroLeft />
				<IntroCenter />
				<IntroRight />
			</div>
		</div>
	)
}
