import Image from 'next/image'

import { IntroAnalyze } from '../../../../public/icons/IntroAnalyze'

export const IntroCenter = () => {
  return (
    <div className='w-[570px] h-[400px] relative md:w-full md:h-[554px]'>
      <div className='w-[522px] h-[157px] bg-white rounded-[40px] absolute inset-x-6 bottom-5 flex items-center justify-center gap-5 p-6 md:w-[90%] md:h-[90%] md:inset-x-5 md:bottom-6 md:flex-col md:text-center'>
        <IntroAnalyze />
        <div className='w-full'>
          <h1 className='text-lg'>Эффективный и безопасный процесс</h1>
          <span className='text-sm tracking-wide'>
            Обеспечивает быструю и безопасную обработку документов или
            транзакций с использованием передовых методов и технологий,
            минимизируя риски и улучшая производительность.
          </span>
        </div>
        <div className='group gap-5 text-[#2F80ED] text-lg cursor-pointer hidden md:flex'>
          Подробнее
          <svg
            className='group-hover:translate-x-3 duration-300'
            xmlns='http://www.w3.org/2000/svg'
            width='28'
            height='28'
            viewBox='0 0 24 24'
            fill='none'
            stroke='currentColor'
            strokeWidth='1'
          >
            <path d='M18 8L22 12L18 16' />
            <path d='M2 12H22' />
          </svg>
        </div>
      </div>
      <Image
        className='md:h-[554px]'
        width={570}
        height={400}
        alt='Intro'
        src='/intro.png'
      />
    </div>
  )
}
