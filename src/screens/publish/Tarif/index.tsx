'use client'

import { Button } from '@nextui-org/react'
import { useRef, useState } from 'react'
import { EffectCoverflow } from 'swiper/modules'
import { Swiper, SwiperSlide } from 'swiper/react'

export const Tariffs = () => {
  const [indexCurrent, setIndexCurrent] = useState(0)
  const swiperRef = useRef<any>(null)

  const handleSlideChange = (swiper: any) => {
    setIndexCurrent(swiper.activeIndex)
  }

  const TARIF_DATA = [
    {
      title: 'Стандарт',
      price: 500,
      descriptions: [
        'Возможность создания и подписания до 10 документов в месяц',
        'Ограниченный доступ к шаблонам документов',
        'Базавоя безопасность и конфеденциальность'
      ]
    },
    {
      title: 'Профессионал',
      price: 1000,
      descriptions: [
        'Возможность создания и подписания до 50 документов в месяц',
        'Достук к расширенным шаблонам документов',
        'Дополнительные опции безопасности, такие как парольная защита'
      ]
    },
    {
      title: 'Премиум',
      price: 500,
      descriptions: [
        'Многопользовательский доступ для команды до 10 человек',
        'Возможность создания и подписания неограниченного количества документов',
        'Полный доступ ко всем шаблонам документов'
      ]
    }
  ]

  return (
    <div className='container w-full h-[800px] relative flex flex-col items-center justify-center m-auto p-10'>
      <h3 className='text-black text-center text-5xl md:text-3xl'>
        Наши <span className='italic font-serif text-[#194BFA]'>тарифы</span>
      </h3>
      <div className='mt-16 w-full'>
        <Swiper
          ref={swiperRef}
          slidesPerView={1}
          breakpoints={{
            1200: {
              slidesPerView: 3,
              spaceBetween: 100
            },
            768: {
              slidesPerView: 1,
              spaceBetween: 30
            }
          }}
          spaceBetween={100}
          initialSlide={1}
          modules={[EffectCoverflow]}
          onSlideChange={handleSlideChange}
          centeredSlides={true}
          effect='coverflow'
          coverflowEffect={{
            rotate: 0,
            stretch: 0,
            depth: 100,
            modifier: 2.5,
            slideShadows: false
          }}
        >
          {TARIF_DATA.map((item: any, index: number) => (
            <SwiperSlide key={index}>
              <div className='h-[500px] w-[350px] rounded-[16px] bg-gradient-to-b from-[#2F80ED] via-[#E580F9] to-[#FF8226] p-[2px]'>
                <div className='h-full w-full rounded-[15px] bg-white px-7 py-10'>
                  <p className='text-xl text-[#194BFA] font-medium'>
                    {item.title}
                  </p>
                  <div className='flex flex-col gap-3 mt-5'>
                    {item.descriptions.map((item: any, index: number) => (
                      <div
                        key={index}
                        className='flex justify-start items-center gap-2 text-md'
                      >
                        <svg
                          className='min-w-[20px] text-[#17933A]'
                          xmlns='http://www.w3.org/2000/svg'
                          width='24'
                          height='24'
                          viewBox='0 0 24 24'
                          fill='none'
                          stroke='currentColor'
                          strokeWidth='2'
                        >
                          <path d='M20 6 9 17l-5-5' />
                        </svg>
                        {item}
                      </div>
                    ))}
                  </div>
                  <p className='text-xl font-medium leading-7 mt-10'>
                    {item.price} сом в месяц
                  </p>
                  <Button className='w-full rounded-2xl bg-[#194BFA] text-white text-sm mt-10'>
                    Выбрать
                  </Button>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
      <div className='w-full flex justify-center mt-10'>
        <div className='w-fit flex justify-between items-center gap-3 cursor-pointer'>
          <div
            onClick={() => swiperRef.current.swiper.slidePrev()}
            className='active:translate-x-[-15px] transition-all duration-300'
          >
            <svg
              width='24'
              height='16'
              viewBox='0 0 24 16'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M0.792391 7.30121C0.402145 7.69201 0.402595 8.32517 0.793396 8.71542L7.16188 15.0749C7.55268 15.4651 8.18584 15.4647 8.57609 15.0739C8.96634 14.6831 8.96589 14.0499 8.57508 13.6596L2.91421 8.00681L8.56705 2.34594C8.9573 1.95514 8.95685 1.32197 8.56604 0.931725C8.17524 0.541478 7.54208 0.541928 7.15183 0.932729L0.792391 7.30121ZM23.4993 6.99219L1.49929 7.00781L1.50071 9.00781L23.5007 8.99219L23.4993 6.99219Z'
                fill='#414141'
              />
            </svg>
          </div>
          {Array.from({ length: TARIF_DATA.length }).map(
            (item: any, index: number) => (
              <div
                key={index}
                className={`bg-[#999999] w-2 h-2 rounded-full transition-all duration-300  ${
                  index === indexCurrent && ' scale-125 !bg-[#194BFA]'
                }`}
              />
            )
          )}
          <div
            onClick={() => swiperRef.current.swiper.slideNext()}
            className='active:translate-x-[15px] transition-all duration-300'
          >
            <svg
              width='24'
              height='16'
              viewBox='0 0 24 16'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                d='M23.2071 8.70711C23.5976 8.31658 23.5976 7.68342 23.2071 7.29289L16.8431 0.928932C16.4526 0.538408 15.8195 0.538408 15.4289 0.928932C15.0384 1.31946 15.0384 1.95262 15.4289 2.34315L21.0858 8L15.4289 13.6569C15.0384 14.0474 15.0384 14.6805 15.4289 15.0711C15.8195 15.4616 16.4526 15.4616 16.8431 15.0711L23.2071 8.70711ZM0.5 9H22.5V7H0.5V9Z'
                fill='#414141'
              />
            </svg>
          </div>
        </div>
      </div>
    </div>
  )
}
