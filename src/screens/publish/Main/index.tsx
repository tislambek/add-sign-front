import Image from 'next/image'

import MainSlug1 from '../../../../public/MainSlug1.webp'
import MainSlug2 from '../../../../public/MainSlug2.webp'
import MainSlug3 from '../../../../public/MainSlug3.webp'
import { ReviewEllips } from '../../../../public/icons/ReviewEllips'

export const Main = () => {
  const CARD_DATA = [
    { title: 'Устранение бумажной рутины', image: MainSlug1 },
    { title: 'Электронные цифровые подписи (ЭЦП)', image: MainSlug2 },
    { title: 'Безопасное хранение данных', image: MainSlug3 }
  ]
  return (
    <div className='w-full h-full relative'>
      <div className='bg-[#194BFA] w-full rounded-[60px] h-[500px] relative overflow-hidden flex items-center justify-center md:h-[1800px] md:rounded-none'>
        <div className='absolute w-[80%] z-10 h-full pt-10 md:w-full'>
          <ReviewEllips />
        </div>
        <div className='w-full text-5xl absolute z-20 top-20 md:text-3xl md:top-10'>
          <h2 className='text-white text-center'>Основные</h2>
          <p className='text-white text-center font-thin italic font-serif mt-6'>
            функциональности
          </p>
        </div>
      </div>
      <div className='w-full flex items-center justify-center md:flex-col absolute z-30 gap-10 top-[50%] md:top-[10%] md:px-5'>
        {CARD_DATA.map((item, index) => (
          <div
            key={index}
            className='w-full h-[500px] rounded-[60px] bg-gradient-to-b from-[#2F80ED] via-[#E580F9] to-[#FF8226] p-[3px] max-w-[400px]'
          >
            <div className='w-full h-full rounded-[60px] bg-white grid grid-cols-1 grid-rows-2 p-3 py-16'>
              <div className='relative w-full h-[300px]'>
                <ReviewEllips />
                <Image
                  width={500}
                  height={500}
                  alt={item.title}
                  src={item.image}
                  className='absolute top-0 right-0 w-full h-full'
                />
              </div>
              <p className='text-[20px] text-center mt-40'>{item.title}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}
