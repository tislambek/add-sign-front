'use client'

import { Button, Input } from '@nextui-org/react'

import { ReviewEllips } from '../../../../public/icons/ReviewEllips'

export const Form = () => {
  return (
    <section className='w-full flex flex-col gap-20'>
      <h3 className='text-black text-center text-5xl font-[400] sm:text-[34px] leading-[45px]'>
        Начните работу в{' '}
        <span className='text-[#194BFA] font-[700] tracking-wider'>
          ADDSIGN
        </span>
      </h3>
      <div className='w-full h-[700px] flex justify-between items-center md:flex-col md:h-full md:gap-5'>
        <div className='w-[50%] h-full bg-[url(/FormImage.png)] bg-no-repeat md:w-[400px] md:h-[330px] bg-cover'></div>
        <div className='w-[50%] h-full relative overflow-hidden flex items-center justify-center md:w-[90%]'>
          <ReviewEllips className='absolute h-full right-[-200px]' />
          <div className='relative h-[550px] w-[450px] rounded-2xl bg-gradient-to-b from-[#2F80ED] via-[#E580F9] to-[#FF8226] p-[3px]'>
            <div className='w-full h-full bg-white rounded-2xl px-5 py-10 text-center'>
              <p>
                Упростите процесс подписания документов! Заполните форму ниже!
              </p>
              <div className='w-full h-fit flex flex-col gap-5 mt-10'>
                <Input
                  isRequired
                  labelPlacement='outside'
                  label='Имя'
                  placeholder='Введите ваше имя'
                  size='lg'
                />
                <Input
                  isRequired
                  labelPlacement='outside'
                  label='Почта'
                  placeholder='Введите почту'
                  size='lg'
                />
                <Input
                  isRequired
                  labelPlacement='outside'
                  label='Номер телефона  *'
                  placeholder='Введите номер телефона'
                  size='lg'
                />
              </div>
              <Button className='mt-10 w-full h-[60px]' color='primary'>
                Отправить
              </Button>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
