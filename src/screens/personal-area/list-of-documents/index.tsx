'use client'

import { FC } from 'react'

import Container from '@/components/ui/container'

import TableDocuments from './TableDocuments'

const ListOfDocumentsContent: FC = () => {
  return (
    <Container className='py-3'>
      <TableDocuments />
    </Container>
  )
}

export default ListOfDocumentsContent
