import {
  Button,
  Checkbox,
  Pagination,
  ScrollShadow,
  Spinner,
  Table,
  TableBody,
  TableCell,
  TableColumn,
  TableHeader,
  TableRow,
  cn,
  useDisclosure
} from '@nextui-org/react'
import Link from 'next/link'
import { Key, useCallback, useMemo, useState } from 'react'
import { BsEye } from 'react-icons/bs'
import { PiPlus } from 'react-icons/pi'

import ModalFollow from '@/components/shared/modal-follow'
import CustomTab from '@/components/ui/custom-tab'

import { normalizeDate } from '@/lib/helper'

import {
  IDocument,
  IDocumentContent
} from '@/store/models/interfaces/document.interfaces'
import {
  useGetIncomintDocumentsQuery,
  useGetOutgoingDocumentsQuery
} from '@/store/services/documents.service'

const columns = [
  { name: 'selection', uid: 'selection' },
  { name: '№', uid: 'id' },
  { name: 'Наименование документа', uid: 'name' },
  { name: 'Дата создания', uid: 'createdAt' },
  { name: 'Статус', uid: 'status' },
  { name: 'Комментаррий', uid: 'comment' },
  { name: 'Действие', uid: 'actions' }
]

const StatusWords = {
  SIGNED: 'Подписанный',
  NEW: 'Созданный'
}

type TWordStatus = typeof StatusWords

const documentVariants = ['Исходящие', 'Входящие']

type TKey = { key: string; id: number }

export default function TableDocuments() {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const [page, setPage] = useState(1)
  const {
    data: outgoing = {
      content: [],
      totalElements: 0,
      totalPages: 0
    },
    isLoading,
    refetch: refetchOutgoing
  } = useGetOutgoingDocumentsQuery({
    page,
    limit: 10
  })

  const {
    data: incoming = {
      content: [],
      totalElements: 0,
      totalPages: 0
    },
    isLoading: isLoadingIncoming,
    refetch: refetchIncoming
  } = useGetIncomintDocumentsQuery({
    page,
    limit: 10
  })

  const [documentIds, setDocumentIds] = useState<TKey[]>([])
  const [toggleDocVariant, setToggleDocVariant] = useState(0)

  const documentData = toggleDocVariant === 0 ? outgoing : incoming

  const isIncoming = toggleDocVariant === 1

  const selectRowHandler = (checked: boolean, documentData: TKey) => {
    if (checked) {
      setDocumentIds(prev => [...prev, documentData])
    } else {
      setDocumentIds(prev => prev.filter(item => item.id !== documentData.id))
    }
  }
  const selectAllRowHandler = (checked: boolean) => {
    if (checked) {
      setDocumentIds(
        documentData.content
          .filter(el => el.document?.status === 'NEW')
          .map(el => ({ id: el.document.id, key: el.document.documentData }))
      )
    } else {
      setDocumentIds([])
    }
  }

  const hasChecked = (id: number) => documentIds.some(item => item.id === id)

  const hasAllChecked =
    documentIds.length ===
    documentData.content.filter(el => el.document?.status === 'NEW').length

  const renderCell = useCallback(
    (documentContent: IDocumentContent, columnKey: Key) => {
      const document = documentContent.document
      const documentStat = documentContent.documentsStats
      const cellValue = document[columnKey as keyof IDocument]
      switch (columnKey) {
        case 'selection':
          return (
            <Checkbox
              isDisabled={document.status !== 'NEW'}
              isSelected={hasChecked(document.id)}
              onValueChange={checked =>
                selectRowHandler(checked, {
                  id: document.id,
                  key: document.documentData
                })
              }
            />
          )
        case 'status':
          return (
            <p
              className={cn('capitalize', {
                'text-success': cellValue === 'SIGNED',
                'text-secondary': cellValue === 'SIGNED',
                'text-default-500': cellValue === 'NEW'
              })}
            >
              {StatusWords[cellValue as keyof TWordStatus]}{' '}
            </p>
          )
        case 'createdAt':
          return normalizeDate(document?.createdAt as string)
        case 'comment':
          return (
            <ScrollShadow className='max-w-[200px] max-h-[100px]'>
              <p>{cellValue || 'Нету комментария'}</p>
            </ScrollShadow>
          )
        case 'actions':
          return (
            <Button
              as={Link}
              href={`/show-document/${document.id}`}
              size='sm'
              endContent={<BsEye />}
              className='text-primary'
            >
              Просмотреть
            </Button>
          )
        default:
          return cellValue
      }
    },
    [documentData?.content?.length, documentIds]
  )

  const bottomContent = useMemo(() => {
    return (
      <div className='py-2 px-2 flex justify-between items-center'>
        {(!isIncoming && (
          <span className='w-[30%] text-small text-default-400'>
            {`${documentIds.length} выбрано из ${documentData.content.length}`}
          </span>
        )) || <div />}
        {documentData.totalPages > 1 && (
          <Pagination
            isCompact
            showControls
            showShadow
            color='primary'
            page={page}
            total={documentData.totalPages}
            onChange={setPage}
          />
        )}
      </div>
    )
  }, [
    documentIds,
    documentData.content.length,
    page,
    toggleDocVariant,
    documentData.totalPages
  ])

  const topContent = useMemo(() => {
    return (
      <div className='w-full flex justify-between gap-2'>
        <CustomTab
          tabContent={documentVariants}
          activeTab={toggleDocVariant}
          handleTabClick={e => {
            setToggleDocVariant(e)
            setPage(1)
            isIncoming ? refetchOutgoing() : refetchIncoming()
          }}
        />
        <div className='flex items-center gap-2'>
          <ModalFollow
            isOpen={isOpen}
            onClose={onClose}
            withoutCreateDocument
            documentIds={documentIds}
            actionAfterSign={() => setDocumentIds([])}
          />
          {!isIncoming && (
            <Button
              onPress={onOpen}
              variant='flat'
              color='primary'
              isDisabled={!new Set(documentIds).size}
            >
              Подписать
            </Button>
          )}
          <Button
            variant='shadow'
            as={Link}
            href='/create-document'
            endContent={<PiPlus />}
            color='primary'
          >
            Создать документ
          </Button>
        </div>
      </div>
    )
  }, [documentIds, documentData.content.length, page, toggleDocVariant, isOpen])

  const filteredColumns = isIncoming
    ? columns.filter(el => el.uid !== 'selection')
    : columns

  return (
    <Table
      isCompact
      aria-label='List of documents'
      isHeaderSticky
      bottomContent={bottomContent}
      bottomContentPlacement='outside'
      topContent={topContent}
      topContentPlacement='outside'
      classNames={{
        wrapper: 'max-h-[calc(100vh_-_215px)]'
      }}
    >
      <TableHeader>
        {filteredColumns.map(column => {
          if (column.uid === 'selection') {
            return (
              <TableColumn key={column.uid} align='center'>
                <Checkbox
                  onValueChange={checked => selectAllRowHandler(checked)}
                  isSelected={hasAllChecked}
                  icon={<PiPlus />}
                />
              </TableColumn>
            )
          }
          return (
            <TableColumn key={column.uid} align='center'>
              {column.name}
            </TableColumn>
          )
        })}
      </TableHeader>
      <TableBody
        isLoading={isLoading || isLoadingIncoming}
        loadingContent={<Spinner />}
        emptyContent={'No documents found'}
      >
        {documentData.content.map(item => (
          <TableRow key={item.document?.id}>
            {filteredColumns.map(column => (
              <TableCell key={column.uid}>
                {renderCell(item, column.uid)}
              </TableCell>
            ))}
          </TableRow>
        ))}
      </TableBody>
    </Table>
  )
}
