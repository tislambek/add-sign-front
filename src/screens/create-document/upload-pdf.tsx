import { Button, cn } from '@nextui-org/react'
import { ChangeEvent, FC, useRef } from 'react'
import { FaFileCircleCheck, FaFilePdf } from 'react-icons/fa6'
import { IoCloseOutline } from 'react-icons/io5'

interface IProps {
  file: File | null
  onChange: (file: File) => void
  onDelete: () => void
  errorMessage?: string
}

const UploadPdf: FC<IProps> = ({ onChange, onDelete, file, errorMessage }) => {
  const fileInputRef = useRef<HTMLInputElement | null>(null)

  const isError = !!errorMessage

  return (
    <div>
      {!file && (
        <div
          className={cn(
            'w-full rounded-medium border-2 border-dashed p-5 flex flex-col gap-3 justify-center items-center',
            {
              'border-danger': isError,
              'border-default-200': !isError
            }
          )}
        >
          <FaFilePdf size={26} color='#184bfa' />
          <span className='font-light text-sm'>Загрузите PDF файл</span>
          <Button
            onClick={() => fileInputRef?.current?.click()}
            size='sm'
            variant='shadow'
            color='primary'
          >
            Загрузить
          </Button>
          <input
            accept='application/pdf'
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              onChange(e.target.files?.[0] as File)
            }
            ref={fileInputRef}
            type='file'
            className='hidden'
          />
        </div>
      )}
      {file && (
        <div className='w-full relative rounded-medium bg-[#f8f8f8] p-6 flex flex-col gap-3 justify-center items-center'>
          <div className='absolute top-1 right-1'>
            <Button onClick={onDelete} isIconOnly variant='light' radius='full'>
              <IoCloseOutline />
            </Button>
          </div>
          <FaFileCircleCheck size={26} color='#184bfa' />
          <span className='font-light text-sm text-center'>
            Загружено файл <br />
            {file?.name}
          </span>
        </div>
      )}
      {errorMessage && (
        <p className='text-xs mt-2 text-danger'>{errorMessage}</p>
      )}
    </div>
  )
}

export default UploadPdf
