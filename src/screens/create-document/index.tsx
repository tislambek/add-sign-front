'use client'

import { Button, Input, Textarea, useDisclosure } from '@nextui-org/react'
import Link from 'next/link'
import { useRouter } from 'next/navigation'
import { FC } from 'react'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import toast from 'react-hot-toast'
import { BiArrowBack } from 'react-icons/bi'

import Container from '@/components/ui/container'

import { getError } from '@/lib/helper'

import {
  usePostDocumentMutation,
  useUploadAttachmentMutation
} from '@/store/services/documents.service'

import ModalFollow from '../../components/shared/modal-follow'
import UploadPdf from './upload-pdf'

export interface IFormValuesCreateDocument {
  name: string
  attachment: File | null
  comment?: string
}

const CreateDocumentContent: FC = () => {
  const router = useRouter()
  const {
    control,
    handleSubmit,
    register,
    setValue,
    watch,
    formState: { errors, isValid }
  } = useForm<IFormValuesCreateDocument>()

  const { isOpen, onOpen, onClose } = useDisclosure()

  const [uploadAttachment, { isLoading: isLoadingUpload }] =
    useUploadAttachmentMutation()
  const [createDocument, { isLoading }] = usePostDocumentMutation()

  const onSubmit: SubmitHandler<IFormValuesCreateDocument> = async values => {
    try {
      const resultUpload = await uploadAttachment(
        values.attachment as File
      ).unwrap()
      await createDocument({
        attachmentId: resultUpload.id,
        comment: values?.comment || '',
        name: values.name
      }).unwrap()
      toast.success('Документ успешно создан')
      router.push('/personal-area/list-of-documents')
    } catch (error) {
      toast.error(getError(error))
    }
  }

  return (
    <Container className='py-5'>
      <Button
        startContent={<BiArrowBack />}
        href='/personal-area/list-of-documents'
        as={Link}
        variant='light'
      >
        Назад к таблице
      </Button>
      <div className='w-full max-w-[600px] p-7 sm:p-4 rounded-2xl mx-auto bg-white shadow-md shadow-[whitesmoke]'>
        <h1 className='text-2xl text-center font-medium'>
          Создать новый документ
        </h1>
        <form
          onSubmit={handleSubmit(onSubmit)}
          className='w-full mt-[40px] flex flex-col gap-5'
        >
          <Input
            {...register('name', { required: 'Пожалуйста, введите это поле' })}
            classNames={{ input: 'placeholder:text-gray-400' }}
            labelPlacement='outside'
            isClearable
            label={
              <p>
                Наименование документа <span className='text-danger'>*</span>
              </p>
            }
            placeholder='Введите наимование документа'
            variant='bordered'
            errorMessage={errors?.name?.message}
            isInvalid={!!errors?.name?.message}
          />

          <Controller
            control={control}
            name='attachment'
            rules={{
              required: 'Пожалуйста, выберите файл',
              validate: value => {
                if (value) {
                  return true
                } else {
                  return 'Пожалуйста, выберите файл'
                }
              }
            }}
            render={({ field: { onChange, value } }) => (
              <UploadPdf
                errorMessage={errors?.attachment?.message}
                onChange={onChange}
                onDelete={() => setValue('attachment', null)}
                file={value}
              />
            )}
          />

          <Textarea
            {...register('comment')}
            classNames={{ input: 'placeholder:text-gray-400' }}
            labelPlacement='outside'
            label='Комментарий'
            placeholder='Напишите комментарий'
            variant='bordered'
          />

          <Button
            isDisabled={!isValid}
            onPress={onOpen}
            variant='bordered'
            type='button'
            className='text-primary'
          >
            Подписать с помощью ЭЦФ
          </Button>
          <Button
            isLoading={isLoading || isLoadingUpload}
            type='submit'
            variant='shadow'
            color='primary'
            className='mt-5'
          >
            Создать
          </Button>
        </form>
      </div>
      <ModalFollow documentData={watch()} onClose={onClose} isOpen={isOpen} />
    </Container>
  )
}

export default CreateDocumentContent
