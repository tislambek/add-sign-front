'use client'

import {
  Button,
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  useDisclosure
} from '@nextui-org/react'
import { Snippet } from '@nextui-org/snippet'
import { FC } from 'react'
import toast from 'react-hot-toast'

import { getError } from '@/lib/helper'

import { useShareDocumentMutation } from '@/store/services/documents.service'

interface IProps {
  documentId: number
}

const ModalCopylink: FC<IProps> = ({ documentId }) => {
  const [shareDocument, { data: link }] = useShareDocumentMutation()
  const { isOpen, onClose, onOpen } = useDisclosure()

  const shareDocumentHandler = async () => {
    try {
      await shareDocument({ id: documentId }).unwrap()
      onOpen()
    } catch (error) {
      toast.error(getError(error))
    }
  }

  return (
    <>
      <Button onPress={shareDocumentHandler} variant='shadow'>
        Поделиться ссылкой
      </Button>
      <Modal
        isOpen={isOpen}
        onClose={onClose}
        backdrop='blur'
        size='lg'
        placement='center'
      >
        <ModalContent>
          <ModalHeader className='flex flex-col gap-1'></ModalHeader>
          <ModalBody className='pb-7'>
            <Snippet color='primary' symbol={null}>
              <span className='whitespace-break-spaces break-all'>
                {typeof window !== 'undefined'
                  ? window.location.host
                  : 'http://176.126.164.136:3000'}
                /view-shared-document/{link?.linkId}
              </span>
            </Snippet>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  )
}

export default ModalCopylink
