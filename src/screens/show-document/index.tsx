'use client'

import { Button, Skeleton, useDisclosure } from '@nextui-org/react'
import dynamic from 'next/dynamic'
import Link from 'next/link'
import { useParams } from 'next/navigation'
import { FC } from 'react'
import { BiArrowBack } from 'react-icons/bi'
import { FaFileCircleCheck } from 'react-icons/fa6'

import ModalFollow from '@/components/shared/modal-follow'
import Container from '@/components/ui/container'

import { normalizeDate } from '@/lib/helper'

import { BASE_URL } from '@/store/interceptor'
import { useGetDocumentByIdQuery } from '@/store/services/documents.service'

import TableFollows from './table-follows'

const ModalCopylink = dynamic(() => import('./modal-copylink'), { ssr: false })

const ShowDocumentContent: FC = () => {
  const { id } = useParams()
  const { isOpen, onOpen, onClose } = useDisclosure()

  const { data: document, isLoading } = useGetDocumentByIdQuery(id as string, {
    skip: !id
  })

  return (
    <Container className='py-5'>
      <Button
        startContent={<BiArrowBack />}
        href='/personal-area/list-of-documents'
        as={Link}
        variant='light'
      >
        Назад к таблице
      </Button>
      <div className='w-full p-7 md:p-1 rounded-2xl mx-auto bg-white shadow-md shadow-[whitesmoke] mt-5'>
        <h1 className='text-2xl text-center font-medium'>Просмотр документа</h1>
        <div className='w-full grid grid-cols-5 items-start mt-5 md:flex md:flex-col'>
          <div className='flex col-span-2 flex-col gap-2 border-r border-r-gray-200 p-2 md:w-full md:border-none'>
            <div className='w-full border-b border-b-gray-200 text-lg font-bold p-2'>
              Информация
            </div>
            <br />
            <div className='w-full space-y-5'>
              <div className='flex flex-col gap-1'>
                <h5 className='font-bold'>Наименование документа</h5>
                {(isLoading && (
                  <Skeleton className='w-[200px] h-3 rounded-md' />
                )) || <p>{document?.name}</p>}
              </div>
              <div className='flex flex-col gap-1'>
                <h5 className='font-bold'>Исходный файл</h5>
                {(isLoading && (
                  <Skeleton className='h-[126px] w-[170px] rounded-medium' />
                )) || (
                  <div className='w-fit relative rounded-medium bg-[#f8f8f8] p-6 flex flex-col gap-3 justify-center items-center'>
                    <FaFileCircleCheck size={26} color='#184bfa' />
                    <span className='font-light text-sm text-center'>
                      Исходный файл <br />
                      <a
                        target='_blank'
                        className='text-primary underline'
                        href={
                          BASE_URL +
                          '/api/v1/attachment?id=' +
                          document?.document?.id
                        }
                      >
                        {document?.document?.name}
                      </a>
                    </span>
                  </div>
                )}
              </div>
              <div className='flex flex-col gap-1'>
                <h5 className='font-bold'>Комментарий</h5>
                {(isLoading && (
                  <div className='flex flex-col gap-1'>
                    <Skeleton className='w-[200px] h-3 rounded-md' />
                    <Skeleton className='w-[300px] h-3 rounded-md' />
                  </div>
                )) || <p>{document?.comment}</p>}
              </div>
              <div className='flex flex-col gap-1'>
                <h5 className='font-bold'>Дата создания</h5>
                {(isLoading && (
                  <Skeleton className='w-[200px] h-3 rounded-md' />
                )) || <p>{normalizeDate(document?.createdAt as string)}</p>}
              </div>
            </div>
          </div>
          <div className='w-full border-b-2 pb-10 md:block hidden'></div>
          <div className='flex flex-col gap-2 p-2 col-span-3 md:max-w-full md:overflow-x-auto md:w-full'>
            <div className='w-full border-b border-b-gray-200 p-2 text-lg font-bold'>
              Подписанты
            </div>
            <br />
            <div className='w-full p-2 md:p-0 md:mb-4'>
              <TableFollows
                isLoading={isLoading}
                signatures={document?.signatures || []}
              />
            </div>
          </div>
        </div>
        <div className='w-full flex justify-end items-center gap-2'>
          <ModalCopylink documentId={document?.id as number} />
          {document?.status === 'NEW' && (
            <Button onPress={onOpen} variant='shadow' color='primary'>
              Подписать
            </Button>
          )}
          <ModalFollow
            onClose={onClose}
            isOpen={isOpen}
            documentIds={[
              {
                id: document?.id as number,
                key: document?.document?.data as string
              }
            ]}
            withoutCreateDocument
          />
        </div>
      </div>
    </Container>
  )
}

export default ShowDocumentContent
