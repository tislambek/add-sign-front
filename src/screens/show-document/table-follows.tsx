import {
  Button,
  Skeleton,
  Table,
  TableBody,
  TableCell,
  TableColumn,
  TableHeader,
  TableRow,
  User
} from '@nextui-org/react'
import Link from 'next/link'
import { FC, Key, useCallback } from 'react'

import ModalViewCms from '@/components/shared/modal-view-cms'

import { BASE_URL } from '@/store/interceptor'
import { ISignature } from '@/store/models/interfaces/document.interfaces'

const columns = [
  { name: 'ФИО', uid: 'fullName' },
  { name: 'Дата подписки', uid: 'createdAt' },
  { name: 'Подпись', uid: 'certificate' }
]

interface IProps {
  signatures: ISignature[]
  isLoading: boolean
}

const TableFollows: FC<IProps> = ({ signatures, isLoading }) => {
  const renderCell = useCallback(
    (user: ISignature, columnKey: Key) => {
      switch (columnKey) {
        case 'fullName':
          return (
            <User
              avatarProps={{ name: user.fullName?.[0] }}
              name={user.fullName}
            >
              {user.fullName}
            </User>
          )
        case 'createdAt':
          return user?.createdAt
        // case 'certificate':
        //   return (
        //     <Button
        //       as={Link}
        //       href={BASE_URL + '/api/v1/attachment?id=' + user?.certificate?.id}
        //       variant='flat'
        //       color='primary'
        //     >
        //       Скачать
        //     </Button>
        //   )
        case 'certificate':
          return <ModalViewCms cms={user.cms} />
        default:
          return
      }
    },
    [signatures]
  )

  return (
    <Table aria-label='Example table with custom cells'>
      <TableHeader columns={columns}>
        {column => (
          <TableColumn key={column.uid} align='center'>
            {column.name}
          </TableColumn>
        )}
      </TableHeader>
      <TableBody
        loadingContent={<Skeleton className='w-full h-full' />}
        emptyContent='Ничего не найдено'
        isLoading={isLoading}
        items={signatures}
      >
        {item => (
          <TableRow key={item.createdAt}>
            {columnKey => <TableCell>{renderCell(item, columnKey)}</TableCell>}
          </TableRow>
        )}
      </TableBody>
    </Table>
  )
}
export default TableFollows
