import { ERROR_CODES, ErrorCodesRu } from '@/store/models/enums/general.enums'

export function getError(error: any) {
  if (Object.keys(error).length === 2 && !isNaN(error?.message)) {
    return ErrorCodesRu[
      ERROR_CODES[
        error.message as keyof typeof ERROR_CODES
      ] as keyof typeof ErrorCodesRu
    ]
  }
  return (
    error?.error ||
    error?.data?.message ||
    error?.message ||
    'Something went wrong'
  )
}

export const normalizeDate = (dateTimeString: string) => {
  const dateTime = new Date(dateTimeString)

  const day = dateTime.getDate().toString().padStart(2, '0')
  const month = (dateTime.getMonth() + 1).toString().padStart(2, '0') // Month starts from 0
  const year = dateTime.getFullYear()
  const hours = dateTime.getHours().toString().padStart(2, '0')
  const minutes = dateTime.getMinutes().toString().padStart(2, '0')
  const seconds = dateTime.getSeconds().toString().padStart(2, '0')

  return `${day}/${month}/${year}   ${hours}:${minutes}:${seconds}`
}

type TSubject = { rdn: string; value: string }

export const makeObject = (array: Array<TSubject>): TSubject => {
  const returnedObj: TSubject = {} as TSubject

  for (let i = 0; i < array.length; i++) {
    const element = array[i]
    returnedObj[element.rdn as keyof typeof element] = element.value
  }

  return returnedObj
}
