'use client'

import { useSearchParams } from 'next/navigation'

import LoginByRutoken from '@/screens/auth/login-by-rutoken'
import MethodAuth from '@/screens/auth/method-auth'
import SignUpByRutoken from '@/screens/auth/sign-up-by-rutoken'

const Auth = () => {
  const params = useSearchParams()

  const isLoginByRutoken = params.get('isLoginByRutoken')
  const isSignUpByRutoken = params.get('isSignUpByRutoken')
  const isLoginByEsia = params.get('isLoginByEsia')
  const isSignUpByEsia = params.get('isSignUpByEsia')

  const isChooseMethod =
    !isLoginByRutoken && !isSignUpByEsia && !isSignUpByRutoken && !isLoginByEsia

  return (
    <div className='w-full p-5 grid grid-cols-2 md:grid-cols-1 bg-white'>
      <div className='bg-[#f8f8f8] md:hidden'></div>
      {isChooseMethod && <MethodAuth />}
      {isSignUpByRutoken && <SignUpByRutoken />}
      {isLoginByRutoken && <LoginByRutoken />}
    </div>
  )
}

export default Auth
