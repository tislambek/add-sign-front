import { Reviews } from '@/screens/publish/Feedback'
import { Form } from '@/screens/publish/Form'
import { Intro } from '@/screens/publish/Intro'
import { Main } from '@/screens/publish/Main'
import { Tariffs } from '@/screens/publish/Tarif'

export default function Home() {
  return (
    <div className='w-full bg-white flex flex-col items-center justify-between py-24 gap-16 md:gap-0'>
      <Intro />
      <Main />
      <Reviews />
      <Tariffs />
      <Form />
    </div>
  )
}
