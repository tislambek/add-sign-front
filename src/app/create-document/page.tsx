import { NextPage } from 'next'

import CreateDocumentContent from '@/screens/create-document'

const CreateDocument: NextPage = () => {
  return <CreateDocumentContent />
}

export default CreateDocument
