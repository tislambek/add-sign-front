import { NextPage } from 'next'

import ShowDocumentContent from '@/screens/show-document'

const ShowDocument: NextPage = () => {
  return <ShowDocumentContent />
}

export default ShowDocument
