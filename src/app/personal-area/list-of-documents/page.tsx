import { NextPage } from 'next'

import ListOfDocumentsContent from '@/screens/personal-area/list-of-documents'

const ListOfDocuments: NextPage = () => {
  return <ListOfDocumentsContent />
}

export default ListOfDocuments
