import ViewSharedDocumentContent from '@/screens/view-shared-document'

const ViewSharedDocument = () => {
  return <ViewSharedDocumentContent />
}

export default ViewSharedDocument
