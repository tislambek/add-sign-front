import { configureStore } from '@reduxjs/toolkit'

import { authApi } from './services/auth.api.service'
import { documentApi } from './services/documents.service'
import authSlice from './slices/auth-slice'

const store = configureStore({
  reducer: {
    [documentApi.reducerPath]: documentApi.reducer,
    [authApi.reducerPath]: authApi.reducer,
    auth: authSlice
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat([documentApi.middleware, authApi.middleware])
})

export default store

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
