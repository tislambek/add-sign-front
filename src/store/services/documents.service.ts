import { createApi } from '@reduxjs/toolkit/query/react'

import { baseQueryWithReauth } from '../interceptor'
import {
  IDocumentContent,
  IPageable,
  IRequestDocument,
  IResUploadFile,
  IResponseDocument
} from '../models/interfaces/document.interfaces'

interface IParams {
  [key: string]: unknown
}

export const documentApi = createApi({
  reducerPath: 'documentApi',
  tagTypes: ['documents', 'document'],
  baseQuery: baseQueryWithReauth,
  endpoints: builder => ({
    getOutgoingDocuments: builder.query<IPageable<IDocumentContent[]>, IParams>(
      {
        query: params => {
          return {
            url: '/api/v1/outgoing-documents',
            method: 'GET',
            params
          }
        },
        providesTags: ['documents']
      }
    ),
    getIncomintDocuments: builder.query<IPageable<IDocumentContent[]>, IParams>(
      {
        query: params => {
          return {
            url: '/api/v1/incoming-documents',
            method: 'GET',
            params
          }
        },
        providesTags: ['documents']
      }
    ),
    getDocumentById: builder.query<IResponseDocument, number | string>({
      query: id => {
        return {
          url: `/api/v1/documents/${id}`,
          method: 'GET'
        }
      },
      providesTags: ['document']
    }),
    getSharedDocumentById: builder.query<IResponseDocument, string>({
      query: link => {
        return {
          url: `/api/v1/shared/link/${link}`,
          method: 'GET'
        }
      },
      providesTags: ['document']
    }),
    postDocument: builder.mutation<IResponseDocument, IRequestDocument>({
      query: body => {
        return {
          url: '/api/v1/documents',
          body,
          method: 'POST'
        }
      },
      invalidatesTags: ['documents']
    }),
    documentSign: builder.mutation<
      null,
      {
        body: {
          documentId: number
          cms: string
        }[]
        isShared?: boolean
      }
    >({
      query: ({ body, isShared }) => {
        return {
          url: `/api/v1/${isShared ? 'shared' : 'documents'}/sign`,
          body,
          method: 'POST'
        }
      },

      invalidatesTags: ['documents', 'document']
    }),
    // documentSignShared: builder.mutation<
    //   null,
    //   {
    //     documentId: number
    //     cms: string
    //   }[]
    // >({
    //   query: data => {
    //     return {
    //       url: '/api/v1/shared/sign',
    //       body: data,
    //       method: 'POST'
    //     }
    //   },

    // }),
    uploadAttachment: builder.mutation<IResUploadFile, File>({
      query: file => {
        const formData = new FormData()
        formData.set('file', file as File)
        return {
          url: '/api/v1/attachment/upload',
          body: formData,
          method: 'POST'
        }
      }
    }),
    shareDocument: builder.mutation<{ linkId: string }, { id: number }>({
      query: params => {
        return {
          url: '/api/v1/documents/link/share',
          params,
          method: 'POST'
        }
      }
    })
  })
})

export const {
  useGetOutgoingDocumentsQuery,
  useGetIncomintDocumentsQuery,
  usePostDocumentMutation,
  useUploadAttachmentMutation,
  useGetDocumentByIdQuery,
  useGetSharedDocumentByIdQuery,
  useDocumentSignMutation,

  useShareDocumentMutation
} = documentApi
