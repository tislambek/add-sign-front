import { createApi } from '@reduxjs/toolkit/query/react'

import { baseQueryWithReauth } from '../interceptor'
import {
  IAuthResponse,
  IRequestSignIn,
  IRequestSignUp,
  IResponseAuthStart,
  IUser
} from '../models/interfaces/auth.interfaces'

export const authApi = createApi({
  reducerPath: 'authApi',
  baseQuery: baseQueryWithReauth,
  endpoints: builder => ({
    signUp: builder.mutation<void, IRequestSignUp>({
      query: body => {
        return {
          url: '/api/v1/register',
          body,
          method: 'POST'
        }
      }
    }),
    signIn: builder.mutation<IAuthResponse, IRequestSignIn>({
      query: body => {
        return {
          url: '/api/v1/login',
          body,
          method: 'POST'
        }
      }
    }),
    signInTest: builder.mutation<any, string>({
      query: pin => {
        return {
          url: '/api/v1/login-test',
          body: pin,
          method: 'POST'
        }
      }
    }),
    authStart: builder.mutation<IResponseAuthStart, void>({
      query: () => {
        return {
          url: '/api/v1/authentication/start',
          method: 'POST'
        }
      }
    }),
    signOut: builder.mutation<void, void>({
      query: () => {
        return {
          url: '/api/v1/sign-out',
          method: 'POST'
        }
      }
    }),
    getUserInfo: builder.query<IUser, void | null>({
      query: () => {
        return {
          url: '/api/v1/user/info'
        }
      }
    })
  })
})

export const {
  useSignUpMutation,
  useSignInTestMutation,
  useAuthStartMutation,
  useSignInMutation,
  useGetUserInfoQuery,
  useSignOutMutation
} = authApi
