import type { PayloadAction } from '@reduxjs/toolkit'
import { createSlice } from '@reduxjs/toolkit'

import { IAuthResponse } from '../models/interfaces/auth.interfaces'

enum StorageKeys {
  TOKEN = '@token'
}

export type TAuth = {
  accessToken: string | null
  refreshToken: string | null
}

const authDataFromLocalStorage =
  typeof window !== 'undefined' &&
  JSON.parse(sessionStorage.getItem(StorageKeys.TOKEN) || '{}')

const initialState: TAuth = {
  accessToken: authDataFromLocalStorage?.accessToken || null,
  refreshToken: authDataFromLocalStorage?.refreshToken || null
}

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    saveAuthResponse: (state, action: PayloadAction<IAuthResponse>) => {
      state.accessToken = action.payload.accessToken
      state.refreshToken = action.payload.refreshToken
      sessionStorage.setItem(StorageKeys.TOKEN, JSON.stringify(action.payload))
    },
    logOut: state => {
      state.accessToken = null
      state.refreshToken = null
      sessionStorage.removeItem(StorageKeys.TOKEN)
    }
  }
})

export const { saveAuthResponse, logOut } = authSlice.actions

export default authSlice.reducer
