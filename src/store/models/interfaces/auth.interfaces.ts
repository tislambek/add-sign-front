export interface IRequestSignUp {
  registrationKey: string
  sign: string
  agreeWithOffer: boolean
}

export interface IRequestSignIn {
  sign: string
  key: string
}

export interface IResponseAuthStart {
  randomToken: string
  key: string
}

export interface IAuthResponse {
  accessToken: string
  tokenType: string
  refreshToken: string
}

export interface IUser {
  id: number
  createdAt: string
  updatedAt: string
  pin: string
  fullName: string
  email: string
  address: string
  position: string
  tin: string
  organizationName: string
  status: any
  certificateInformation: CertificateInformation
  role: string
}

export interface CertificateInformation {
  version: any
  algorithm: any
  issueDate: string
  expirationDate: string
  issuer: string
  serialNumber: number
}
