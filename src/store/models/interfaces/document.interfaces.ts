export interface IRequestDocument {
  name: string
  attachmentId: number
  comment: string
}
export interface IResUploadFile {
  id: number
  createdAt: string
  updatedAt: string
  name: string
  type: string
  data: string
}

export interface IResponseDocument {
  id: number
  createdAt: string
  updatedAt: string
  name: string
  attachment: IResUploadFile
  document: IResUploadFile
  status: string
  comment: string
  signatures: ISignature[]
}

export interface ISignature {
  createdAt: string
  fullName: string
  cms: string
  certificate: {
    id: number
    name: string
    data: string
  }
}

export interface IDocument {
  id: 1
  name: string
  createdAt: string
  comment: string
  status: string
  documentData: string
}

export interface IDocumentStat {
  total: number
  signed: number
}

export interface IDocumentContent {
  document: IDocument
  documentsStats: IDocumentStat
}

export interface IPageable<T> {
  content: T
  pageable: {
    pageNumber: number
    pageSize: number
    sort: {
      sorted: boolean
      empty: boolean
      unsorted: boolean
    }
    offset: number
    paged: boolean
    unpaged: boolean
  }
  last: boolean
  totalPages: number
  totalElements: number
  first: boolean
  size: number
  number: number
  sort: {
    sorted: boolean
    empty: boolean
    unsorted: boolean
  }
  numberOfElements: number
  empty: boolean
}
