export const ReviewEllips = ({ className }: { className?: string }) => {
  return (
    <svg
      className={className}
      viewBox='0 0 788 703'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M574.925 351.946C574.925 442.52 492.128 516.044 389.862 516.044C287.596 516.044 204.798 442.52 204.798 351.946C204.798 261.372 287.596 187.848 389.862 187.848C492.128 187.848 574.925 261.372 574.925 351.946Z'
        stroke='url(#paint0_linear_135_4553)'
        strokeOpacity='0.5'
      />
      <path
        d='M610.611 351.946C610.611 459.764 511.837 547.269 389.862 547.269C267.886 547.269 169.113 459.764 169.113 351.946C169.113 244.128 267.886 156.623 389.862 156.623C511.837 156.623 610.611 244.128 610.611 351.946Z'
        stroke='url(#paint1_linear_135_4553)'
        strokeOpacity='0.5'
      />
      <path
        d='M644.511 351.053C644.511 476.123 531.552 577.601 392.092 577.601C252.633 577.601 139.673 476.123 139.673 351.053C139.673 225.984 252.633 124.506 392.092 124.506C531.552 124.506 644.511 225.984 644.511 351.053Z'
        stroke='url(#paint2_linear_135_4553)'
        strokeOpacity='0.5'
      />
      <path
        d='M680.197 351.946C680.197 493.766 551.462 608.827 392.538 608.827C233.614 608.827 104.879 493.766 104.879 351.946C104.879 210.127 233.614 95.0664 392.538 95.0664C551.462 95.0664 680.197 210.127 680.197 351.946Z'
        stroke='url(#paint3_linear_135_4553)'
        strokeOpacity='0.5'
      />
      <path
        d='M714.99 351.946C714.99 510.518 571.276 639.159 393.876 639.159C216.476 639.159 72.7627 510.518 72.7627 351.946C72.7627 193.374 216.476 64.7334 393.876 64.7334C571.276 64.7334 714.99 193.374 714.99 351.946Z'
        stroke='url(#paint4_linear_135_4553)'
        strokeOpacity='0.5'
      />
      <path
        d='M749.783 345.256C749.783 517.861 590.103 657.894 392.984 657.894C195.866 657.894 36.1853 517.861 36.1853 345.256C36.1853 172.65 195.866 32.6172 392.984 32.6172C590.103 32.6172 749.783 172.65 749.783 345.256Z'
        stroke='url(#paint5_linear_135_4553)'
        strokeOpacity='0.5'
      />
      <path
        d='M787.253 351.5C787.253 545.3 611.187 702.5 393.876 702.5C176.566 702.5 0.5 545.3 0.5 351.5C0.5 157.7 176.566 0.5 393.876 0.5C611.187 0.5 787.253 157.7 787.253 351.5Z'
        stroke='url(#paint6_linear_135_4553)'
        strokeOpacity='0.5'
      />
      <defs>
        <linearGradient
          id='paint0_linear_135_4553'
          x1='218.292'
          y1='187.348'
          x2='302.588'
          y2='553.045'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#2F80ED' />
          <stop offset='0.53' stopColor='#E580F9' />
          <stop offset='0.985' stopColor='#FF8226' />
        </linearGradient>
        <linearGradient
          id='paint1_linear_135_4553'
          x1='185.297'
          y1='156.123'
          x2='285.388'
          y2='591.29'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#2F80ED' />
          <stop offset='0.53' stopColor='#E580F9' />
          <stop offset='0.985' stopColor='#FF8226' />
        </linearGradient>
        <linearGradient
          id='paint2_linear_135_4553'
          x1='158.245'
          y1='124.006'
          x2='275.782'
          y2='627.834'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#2F80ED' />
          <stop offset='0.53' stopColor='#E580F9' />
          <stop offset='0.985' stopColor='#FF8226' />
        </linearGradient>
        <linearGradient
          id='paint3_linear_135_4553'
          x1='126.109'
          y1='94.5664'
          x2='258.746'
          y2='666'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#2F80ED' />
          <stop offset='0.53' stopColor='#E580F9' />
          <stop offset='0.985' stopColor='#FF8226' />
        </linearGradient>
        <linearGradient
          id='paint4_linear_135_4553'
          x1='96.5155'
          y1='64.2334'
          x2='244.993'
          y2='702.908'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#2F80ED' />
          <stop offset='0.53' stopColor='#E580F9' />
          <stop offset='0.985' stopColor='#FF8226' />
        </linearGradient>
        <linearGradient
          id='paint5_linear_135_4553'
          x1='62.6292'
          y1='32.1172'
          x2='221.27'
          y2='728.67'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#2F80ED' />
          <stop offset='0.53' stopColor='#E580F9' />
          <stop offset='0.985' stopColor='#FF8226' />
        </linearGradient>
        <linearGradient
          id='paint6_linear_135_4553'
          x1='29.7021'
          y1='-1.65435e-06'
          x2='210.701'
          y2='780.467'
          gradientUnits='userSpaceOnUse'
        >
          <stop stopColor='#2F80ED' />
          <stop offset='0.53' stopColor='#E580F9' />
          <stop offset='0.985' stopColor='#FF8226' />
        </linearGradient>
      </defs>
    </svg>
  )
}
